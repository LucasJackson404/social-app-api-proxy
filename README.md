# Scoial App API proxy

NGINX proxy app for our social app API

## Usage

### Environement Variables

* 'LISTEN_PORT' - Port to listen on (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app)
* 'APP_PORT' - Port of the app to forward requests to (default: '8000')